package com.mycompany.allswing;

import javax.swing.*;

public class JavaJOptionPane3 {

    JFrame f;

    JavaJOptionPane3() {
        f = new JFrame();
        String name = JOptionPane.showInputDialog(f, "Enter Name");
    }

    public static void main(String[] args) {
        new JavaJOptionPane3();
    }
}
