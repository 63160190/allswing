package com.mycompany.allswing;

import javax.swing.*;  
public class JavaJSliderpaintingticks extends JFrame{  
public JavaJSliderpaintingticks() {  
JSlider slider = new JSlider(JSlider.HORIZONTAL, 0, 50, 25);  
slider.setMinorTickSpacing(2);  
slider.setMajorTickSpacing(10);  
slider.setPaintTicks(true);  
slider.setPaintLabels(true);  
  
JPanel panel=new JPanel();  
panel.add(slider);  
add(panel);  
}  
public static void main(String s[]) {  
JavaJSliderpaintingticks frame=new JavaJSliderpaintingticks();  
frame.pack();  
frame.setVisible(true);  
}  
}  