package com.mycompany.allswing;

import javax.swing.*;    
public class JavaJTable {    
    JFrame f;    
    JavaJTable(){    
    f=new JFrame();    
    String data[][]={ {"101","Mark","670000"},    
                          {"102","May","780000"},    
                          {"101","Mew","700000"}};    
    String column[]={"ID","NAME","SALARY"};         
    JTable jt=new JTable(data,column);    
    jt.setBounds(30,40,200,300);          
    JScrollPane sp=new JScrollPane(jt);    
    f.add(sp);          
    f.setSize(300,400);    
    f.setVisible(true);    
}     
public static void main(String[] args) {    
    new JavaJTable();    
}    
}  